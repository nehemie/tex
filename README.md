Latex: Bits of code
====

This repository contains bits of Tex code and is available under a CC BY 4.0 License (see License.md). 
If you find some of this code useful and improve it, share it alike any times you can.

## Contents
  - [Timetable for project](tabular#timetable-for-project-or-grant)
  - [Letter: french template](letters#french-template)
  - [Scripts (bash and vim based)](script#)
