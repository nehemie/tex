Readme
--------------

## Timetable for a project or grant proposal 

 - [TimetableProject.tex](TimetableProject.tex) (compile with XeLaTex - see comments in .tex files)

It is often welcome to add a calendar in a proposal to visualise the planning. I propose here two variations based on the same principle (pdf preview: [TimetableProject.pdf](TimetableProject.pdf). The first timetable is for a 4 years thesis the second one for a short grant of 10 months.

Unfortunately when I wrote the code of the table (early in 2012), I didn't documented how I started but most probably I used this answer from Gonzalo Medina on TexStackexchange: http://tex.stackexchange.com/a/32689